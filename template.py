

# ----------------------------------------------------------------------
def get_action(eeg_data):
    """"""
    # Aquí se deben de procesar los datos de EEG
    # para determinar cual debería ser la acción
    # que se debe retornar.

    print(f"EEG shape: {eeg_data.shape}")

    # Solo son válidas las acciones: 'North', 'South', 'East', 'West'
    action = 'East'

    return action
