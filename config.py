
MODE = 'pacman'
# MODE = 'arrows'
TRIALS = 4
IP_RASP = '192.168.1.1'
IP_BOARD = '192.168.1.113'
# ACTIONS = ['north', 'south', 'east', 'west']
ACTIONS = ['east', 'west']
ACQUISITION = False
MONTAGE = ['Fp1', 'Fp2', 'F3', 'Fz', 'F4', 'T3', 'C3', 'Cz', 'C4', 'T4', 'P3', 'Pz', 'P4', 'O1', 'Oz']
