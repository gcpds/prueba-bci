
import sys
from datetime import datetime

from config import *
if ACQUISITION:
    # from openbci.acquisition import CytonRFDuino
    from openbci.stream.ws import CytonWS


# montage = "Fp1 Fp2 F3 Fz F4 T3 C3 Cz C4 T4 P3 Pz P4 O1 O2".split()
montage = MONTAGE

if ACQUISITION:
    openbci = CytonWS(ip=IP_RASP, montage=montage, device_id=IP_BOARD, boardmode='BOARD_MODE_MARKER', sample_rate='SAMPLE_RATE_500HZ',)
    # openbci = CytonRFDuino(montage=montage)
    # openbci.command(openbci.BOARD_MODE_MARKER)


import gym
import gym_bci

environment_config = {
    'maintain': 4000,
    'min_delay': 800,
    'max_delay': 1200,
    'button_start': True,
}

env = gym.make(f'bci-{MODE}-v0')
env.reset(chosenLayout="train")


markers_for_actions = {
    'east': 110,
    'west': 111,
    'north': 112,
    'south': 113,
}


session_config = {

    'trials': TRIALS,
    'start_marker': 100,
    'end_marker': 101,

}

import numpy as np

# actions = list(markers_for_actions.keys())


actions_ = {
    'north': (11, 1),
    'south': (11, 20),
    'east': (1, 11),
    'west': (20, 11),
}

actions = []
for action in ACTIONS:
    actions.append((action, actions_[action]))

trials_per_action = session_config['trials'] // len(actions)


cues = np.array([[action] * trials_per_action for action in actions]).reshape(-1).reshape(-1, 2)

if not cues.shape[0]:
    print('No hay suficientes trials')
    sys.exit()

np.random.shuffle(cues)


import time
from datetime import datetime

now = datetime.now()
now = str(now).replace(':', '_')

# start acquisition
if ACQUISITION:
    openbci.start_collect()
    openbci.wait_for_data()
    time.sleep(1)  # pause for ensuring the device initialization
    openbci.send_marker(session_config['start_marker'], 7)
    time.sleep(1)  # other pause for separate the first marker

# cues
for cue, start in cues:
    if ACQUISITION:
        openbci.send_marker(markers_for_actions[cue], 7)  # send the cue marker

    if MODE == 'pacman':
        env.step('move', pacman_location=start)  # display the cue
        time.sleep(0.5)
        [env.step(cue, delay=0.1) for i in range(20)]

        sl = np.random.randint(environment_config['min_delay'], environment_config['max_delay'])
        time.sleep(sl / 1000)

    elif MODE == 'arrows':
        env.step(cue)  # display the cue

    # time.sleep(2)
    # env.step(cue)  # display the cue

if not ACQUISITION:
    sys.exit()

# stop acquisition
openbci.send_marker(session_config['end_marker'], 7)
time.sleep(10)
openbci.stop_collect()
time.sleep(3)  # pause for ensuring data storage

# close stimuli window
env.close()
openbci.close()


eeg_data = np.array(openbci.eeg_buffer.queue).T
sample, eeg, aux, footer, timestamp = eeg_data
eeg = np.stack(eeg).T
eeg.shape

time = np.linspace(0, (datetime.fromtimestamp(timestamp[-1]) - datetime.fromtimestamp(timestamp[0])).total_seconds(), eeg.shape[1])

from matplotlib import pyplot
import matplotlib as mpl
mpl.rcParams.update(mpl.rcParamsDefault)

figure = pyplot.figure(figsize=(16, 9), dpi=90)
gridsize = (3, 1)

ax1 = pyplot.subplot2grid(gridsize, (0, 0), rowspan=2)
pyplot.title(f"Motor Imagery - {now}")

for i, eeg_ in enumerate(eeg):
    pyplot.plot(time, eeg_ + i)

# ax1.set_yticks(list(openbci.montage.keys()))
# ax1.set_yticklabels(openbci.montage.values())

ax2 = pyplot.subplot2grid(gridsize, (2, 0))
pyplot.plot(time, aux)
pyplot.ylim(90, 160)

start = (np.where(aux == session_config['start_marker'])[0])
end = (np.where(aux == session_config['end_marker'])[0])
markers_points = [(k, np.where(aux == markers_for_actions[k])[0]) for k in markers_for_actions]

pyplot.plot(time[start], aux[start], 'o', label='Start')
pyplot.plot(time[end], aux[end], 'o', label='End')

for marker, points in markers_points:
    pyplot.plot(time[points], aux[points], 'o', label=marker.capitalize())

pyplot.xlabel("Time [s]")

pyplot.grid(True)
pyplot.legend()
pyplot.show()

import os
import json

# filename
if not os.path.exists('data'):
    os.mkdir('data')
filename_path = os.path.join('data', now)

# save figure
figure.savefig(f"{filename_path}.png")

# save csv
to_save = np.concatenate([eeg, np.stack([aux])]).T
np.savetxt(f"{filename_path}.csv", to_save, delimiter=',')

# save json
with open(f"{filename_path}.json", 'w') as fp:

    data = environment_config.copy()
    data.update({f"{k}_marker": markers_for_actions[k] for k in markers_for_actions})
    data.update(session_config)

    data['datetime'] = now.replace('_', ':')
    data['sample_rate'] = f"{aux.shape[0]/time[-1]:.2f} Hz"
    # data['channels'] = ", ".join(openbci.montage.values())
    data["channels_system"] = "10-20"

    json.dump(data, fp, indent=4)

print(f"saved as: {filename_path}")
