from openbci.stream.ws import CytonWS_decorator
from matplotlib import pyplot
from datetime import datetime
import numpy as np
import mne
from openbci.preprocess import eeg_filters, eeg_features
import time
# import logging
# logging.getLogger().setLevel(logging.INFO)

import matplotlib
matplotlib.use('TkAgg')

import warnings
warnings.filterwarnings('ignore')


pyplot.figure(figsize=(16, 9), dpi=90)
pyplot.ion()

N = 10

eeg_data = np.zeros((15, 1000 * N))
timestamp_data = np.zeros(1000 * N)
aux_data = np.zeros(1000 * N)

from config import *


config = dict(

    # force=True,

    # ip='192.168.1.1',
    ip=IP_RASP,
    # device_id='192.168.1.113',
    device_id=IP_BOARD,
    port="8845",
    mode='wifi',
    pack_size=1000,
    boardmode='BOARD_MODE_MARKER',
    # sample_rate='SAMPLE_RATE_250HZ',
    sample_rate='SAMPLE_RATE_500HZ',
    # sample_rate='SAMPLE_RATE_1KHZ',
    montage=MONTAGE
)


montage = mne.channels.make_standard_montage('standard_1020')
info = mne.create_info(config['montage'], sfreq=1000, ch_types="eeg", montage=montage)


@CytonWS_decorator(**config)
def eeg_with_ap(ws, data):
    global eeg_data, timestamp_data, aux_data

    print(f"ID: {data['pack_id']}")
    if data['pack_id'] < 0:
        return

    samp, eeg, aux, footer, timestamp = data['data']
    eeg = np.array(eeg).T
    # timestamp = np.array(timestamp)

    try:
        eeg = eeg_filters.band440(eeg, timestamp=timestamp)
    except:
        pass

    s = eeg.shape[1]

    eeg_data = np.roll(eeg_data, -s, axis=1)
    eeg_data[:, -s:] = eeg

    timestamp_data = np.linspace(-N, 0, eeg_data.shape[1])

    ax1 = pyplot.subplot(221, label='1')
    # ax1 = pyplot.subplot2grid((2, 3), (0, 0))
    pyplot.cla()
    for i, ch in enumerate(eeg_data):
        pyplot.plot(timestamp_data, (5 * ch) + i)

    ax1.set_yticks(range(0, len(config['montage'])))
    ax1.set_yticklabels(config['montage'])

    pyplot.xlabel('Time [$s$]')

    # ax3 = pyplot.subplot2grid((2, 3), (0, 1))
    pyplot.subplot(222, label='2')
    pyplot.cla()

    aux = np.array(aux)
    s = aux.shape[0]
    aux_data = np.roll(aux_data, -s, axis=0)
    aux_data[-s:] = aux

    pyplot.plot(timestamp_data, aux_data)
    pyplot.ylim(90, 160)

    # pyplot.subplot(223)
    # ax2 = pyplot.subplot2grid((2, 3), (1, 0))
    pyplot.subplot(223, label='3')
    pyplot.cla()

    eeg_ = eeg ** 2

    values = eeg_ - eeg_.mean(axis=0)
    values = eeg_.mean(axis=1)

    mne.viz.plot_topomap(values, info, names=config['montage'], show_names=True)

    # pyplot.subplot(224)
    # ax4 = pyplot.subplot2grid((2, 3), (1, 1), colspan=2)
    pyplot.subplot(224, label='4')
    pyplot.cla()

    f, w = eeg_features.welch(eeg, timestamp=timestamp)
    for w_ in w:
        pyplot.plot(f, w_)
    pyplot.xlim(0, 50)
    pyplot.legend(config['montage'], loc='upper center', ncol=8)
    pyplot.xlabel('Frequency [$Hz$]')

    pyplot.pause(0.001)

    return


