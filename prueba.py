from config import *

import numpy as np

import gym
import gym_bci

env = gym.make(f'bci-{MODE}-v0')
env.reset(chosenLayout='openClassic', no_ghosts=False)

from template import get_action

# openbci = CytonRFDuino(port="COM0")  # conectar con OpenBCI
# openbci = CytonRFDuino()

if ACQUISITION:
    from openbci.stream.ws import CytonWS
    openbci = CytonWS(ip=IP_RASP, montage=montage, device_id=IP_BOARD, boardmode='BOARD_MODE_MARKER', sample_rate='SAMPLE_RATE_500HZ',)
    openbci.start_collect()  # iniciar la adquisición de datos
    openbci.wait_for_data()


while True:
# for i in range(5):
    if ACQUISITION:
        while not openbci.eeg_pack.qsize():  # si el paquete de 1000 milisegundos no está listo
            time.sleep(0.03)  # espere

        # cuando ya esté listo
        eeg_data = np.array(openbci.eeg_pack.get())  # convertirlo a un array de numpy
        eeg_data = eeg_data[0]  # orientar la matriz para que quede de la forma canales X tiempo (16 X muestras)

    else:
        eeg_data = np.random.random((15, 1000))

    action = get_action(eeg_data)  # enviar los datos del EEG a la función de discriminación para optener la acción a ejecutar

    try:
        pacman = env.step(action)  # ejecutar la acción
    except:
        break
    if pacman:
        _, _, done, _ = pacman
        if done:
            # `done` es verdadero cuando el juego se termina, esto es, cuando se gana o se pierde.
            break


if ACQUISITION:
    openbci.stop_collect()  # detener la recolección de datos
env.close()
print("Stop")
